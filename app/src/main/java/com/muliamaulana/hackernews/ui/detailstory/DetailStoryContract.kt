package com.muliamaulana.hackernews.ui.detailstory

import com.muliamaulana.hackernews.apiservices.response.ArticleResponse
import com.muliamaulana.hackernews.apiservices.response.CommentResponse
import com.muliamaulana.hackernews.database.FavoriteStory

interface DetailStoryContract {

    interface View {
        fun hideLoading()
        fun showLoading()
        fun showError(errorMessage: String?)
        fun getArticle(article: ArticleResponse)
        fun getCommentsList(comment: CommentResponse)
        fun setFavoriteState(favStoryList: List<FavoriteStory>)
    }

    interface Presenter {
        fun getArticle(id: Int)
        fun getComment(id: Int)
        fun onDestroyPresenter()
        fun insertFavStory(id: Int)
        fun checkIsFavStory(id: Int)
        fun deleteFavStory(id: Int)
    }

}