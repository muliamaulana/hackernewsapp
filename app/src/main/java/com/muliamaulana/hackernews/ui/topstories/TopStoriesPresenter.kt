package com.muliamaulana.hackernews.ui.topstories

import com.muliamaulana.hackernews.apiservices.response.ArticleResponse
import com.muliamaulana.hackernews.repository.HackerNewsRepositoryImpl
import com.muliamaulana.hackernews.repository.LocalRepositoryImpl
import com.muliamaulana.hackernews.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class TopStoriesPresenter(
    private val mView: TopStoriesContract.View,
    private val topStorRepositoryImpl: HackerNewsRepositoryImpl,
    private val localRepositoryImpl: LocalRepositoryImpl,
    private val scheduler: SchedulerProvider
) : TopStoriesContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getTopStories() {
        compositeDisposable.add(
            topStorRepositoryImpl.getTopStories().observeOn(scheduler.ui())
                .subscribeOn(scheduler.io()).subscribe({
                    mView.hideLoading()
                    mView.getTopStoriesList(it)
                }, {
                    mView.hideLoading()
                    mView.showError(it.message)
                })
        )
    }

    override fun getArticle(id: Int) {
        mView.showLoading()
        compositeDisposable.add(
            topStorRepositoryImpl.getArticle(id).observeOn(scheduler.ui()).subscribeOn(
                scheduler.io()
            ).subscribe({
                mView.hideLoading()
                mView.getArticleList(it)
            }, {
                mView.hideLoading()
                mView.showError(it.message)
            })
        )
    }

    override fun getFavorite() {
        val favList = localRepositoryImpl.getFavoritesFromDB()
        val articleList: MutableList<ArticleResponse> = mutableListOf()
        if (favList.isNotEmpty()) {
            compositeDisposable.add(
                topStorRepositoryImpl.getArticle(favList[favList.lastIndex].idStory)
                    .observeOn(scheduler.ui()).subscribeOn(scheduler.io()).subscribe({
                        mView.showFavorie(it)
                    }, {
                        mView.showError(it.message)
                    })
            )
        } else mView.showLastViewed()

    }

    override fun getLastViewed(id: Int) {
        compositeDisposable.add(
            topStorRepositoryImpl.getArticle(id).observeOn(scheduler.ui()).subscribeOn(
                scheduler.io()
            ).subscribe({
                mView.hideLoading()
                mView.showArticleLastViewd(it)
            }, {
                mView.hideLoading()
                mView.showError(it.message)
            })
        )
    }
}