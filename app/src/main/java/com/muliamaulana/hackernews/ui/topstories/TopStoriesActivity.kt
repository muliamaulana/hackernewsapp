package com.muliamaulana.hackernews.ui.topstories

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muliamaulana.hackernews.R
import com.muliamaulana.hackernews.adapter.TopStoriesAdapter
import com.muliamaulana.hackernews.apiservices.HackerNewsServices
import com.muliamaulana.hackernews.apiservices.response.ArticleResponse
import com.muliamaulana.hackernews.repository.HackerNewsRepositoryImpl
import com.muliamaulana.hackernews.repository.LocalRepositoryImpl
import com.muliamaulana.hackernews.utils.AppSchedulerProvider
import kotlinx.android.synthetic.main.activity_main.*


class TopStoriesActivity : AppCompatActivity(),
    TopStoriesContract.View {

    private var topStories: MutableList<Int> = mutableListOf()
    private var articleList: MutableList<ArticleResponse> = mutableListOf()
    private lateinit var mPresenter: TopStoriesPresenter
    private val hackerNewsServices by lazy {
        HackerNewsServices.getHackerNewsServices()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.title = "Top Stories"

        val service = hackerNewsServices
        val request = HackerNewsRepositoryImpl(service)
        val localRepository = LocalRepositoryImpl(applicationContext)
        val scheduler = AppSchedulerProvider()
        mPresenter = TopStoriesPresenter(this, request, localRepository, scheduler)
        mPresenter.getTopStories()
        mPresenter.getFavorite()

    }

    override fun hideLoading() {
        progress_top_stories.visibility = View.GONE
    }

    override fun showLoading() {
        progress_top_stories.visibility = View.VISIBLE
    }

    override fun showError(errorMessage: String?) {
        Toast.makeText(this, "Error: $errorMessage", Toast.LENGTH_LONG).show()
    }

    override fun getTopStoriesList(topStoriesList: List<Int>) {
        topStories.clear()
        articleList.clear()
        topStories.addAll(topStoriesList)
        for (i in 0 until topStories.size) {
            mPresenter.getArticle(topStoriesList[i])
        }
    }

    override fun getArticleList(article: ArticleResponse) {
        articleList.add(article)
        val layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        recyclewview_top_stories.layoutManager = layoutManager
        recyclewview_top_stories.adapter = TopStoriesAdapter(articleList, this)

    }

    override fun onResume() {
        super.onResume()
        mPresenter.getFavorite()
    }

    override fun showFavorie(article: ArticleResponse) {
        layout_favorite.visibility = View.VISIBLE
        textview_title_last.text = "Last favorite"
        textview_title_last_favorite.text = article.title
    }

    override fun showLastViewed() {
        val sharedPreferences = getSharedPreferences("last_viewed_story", Context.MODE_PRIVATE)
        val id = sharedPreferences.getInt("last_id", -1)
        if (id!= -1){
            mPresenter.getLastViewed(id)
        } else {
            layout_favorite.visibility = View.GONE
        }
    }

    override fun showArticleLastViewd(article: ArticleResponse) {
        layout_favorite.visibility = View.VISIBLE
        textview_title_last.text = "Last viewed"
        textview_title_last_favorite.text = article.title
    }
}
