package com.muliamaulana.hackernews.ui.detailstory

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muliamaulana.hackernews.R
import com.muliamaulana.hackernews.adapter.CommentAdapter
import com.muliamaulana.hackernews.apiservices.HackerNewsServices
import com.muliamaulana.hackernews.apiservices.response.ArticleResponse
import com.muliamaulana.hackernews.apiservices.response.CommentResponse
import com.muliamaulana.hackernews.database.FavoriteStory
import com.muliamaulana.hackernews.repository.HackerNewsRepositoryImpl
import com.muliamaulana.hackernews.repository.LocalRepositoryImpl
import com.muliamaulana.hackernews.utils.AppSchedulerProvider
import com.muliamaulana.hackernews.utils.Utils
import kotlinx.android.synthetic.main.activity_detail_story.*

class DetailStoryActivity : AppCompatActivity(), DetailStoryContract.View {

    private var commentList: MutableList<CommentResponse> = mutableListOf()
    private lateinit var mPresenter: DetailStoryPresenter
    private val hackerNewsServices by lazy {
        HackerNewsServices.getHackerNewsServices()
    }
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var idStory: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_story)
        supportActionBar?.title = "Story Detail"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val service = hackerNewsServices
        val request = HackerNewsRepositoryImpl(service)
        val localRepository = LocalRepositoryImpl(applicationContext)
        val scheduler = AppSchedulerProvider()
        mPresenter = DetailStoryPresenter(this, request, localRepository, scheduler)

        idStory = intent.getIntExtra("id", 0)
        mPresenter.getArticle(idStory)
        mPresenter.checkIsFavStory(idStory)
    }

    override fun hideLoading() {
        layout_detail.visibility = View.VISIBLE
        progress_detail.visibility = View.GONE
    }

    override fun showLoading() {
        layout_detail.visibility = View.GONE
        progress_detail.visibility = View.VISIBLE
    }

    override fun showError(errorMessage: String?) {
        Toast.makeText(this, "Error: $errorMessage", Toast.LENGTH_LONG).show()
    }

    override fun getArticle(article: ArticleResponse) {
        textview_detail_title.text = article.title
        textview_detail_author.text = "by " + article.by
        if (article.time != null) {
            textview_detail_date.text = Utils.getDateTime(article.time)
        }

        commentList.clear()
        val commentCount = article.kids?.size
        if (commentCount != null && commentCount != 0) {
            for (i in 0 until commentCount)
                mPresenter.getComment(article.kids[i]!!)

        }
    }

    override fun getCommentsList(comment: CommentResponse) {
        commentList.add(comment)
        val adapter = CommentAdapter(commentList, this)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclewview_comments.layoutManager = layoutManager
        recyclewview_comments.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun setFavoriteState(favStoryList: List<FavoriteStory>) {
        if (!favStoryList.isEmpty()) isFavorite = true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_detail, menu)
        menuItem = menu
        setFavorite()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.action_add_to_favorite -> {
                isFavorite = if (!isFavorite) {
                    mPresenter.insertFavStory(idStory)
                    Toast.makeText(this, "Added  to favorite", Toast.LENGTH_SHORT).show()
                    true
                } else {
                    mPresenter.deleteFavStory(idStory)
                    Toast.makeText(this, "Removed from favorite", Toast.LENGTH_SHORT).show()
                    false
                }
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_star)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_star_border)
    }


}
