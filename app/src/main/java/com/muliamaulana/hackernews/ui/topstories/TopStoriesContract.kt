package com.muliamaulana.hackernews.ui.topstories

import com.muliamaulana.hackernews.apiservices.response.ArticleResponse

interface TopStoriesContract {
    interface View {
        fun hideLoading()
        fun showLoading()
        fun showError(errorMessage: String?)
        fun getTopStoriesList(topStoriesList: List<Int>)
        fun getArticleList(article: ArticleResponse)
        fun showFavorie(article: ArticleResponse)
        fun showLastViewed()
        fun showArticleLastViewd(article: ArticleResponse)
    }

    interface Presenter {
        fun getTopStories()
        fun getArticle(id: Int)
        fun getFavorite()
        fun getLastViewed(id: Int)
    }

}