package com.muliamaulana.hackernews.ui.detailstory

import com.muliamaulana.hackernews.repository.HackerNewsRepositoryImpl
import com.muliamaulana.hackernews.repository.LocalRepository
import com.muliamaulana.hackernews.repository.LocalRepositoryImpl
import com.muliamaulana.hackernews.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class DetailStoryPresenter(
    private val mView: DetailStoryContract.View,
    private val hackerNewsRepositoryImpl: HackerNewsRepositoryImpl,
    private val localRepositoryImpl: LocalRepositoryImpl,
    private val scheduler: SchedulerProvider
) : DetailStoryContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getArticle(id: Int) {
        mView.showLoading()
        compositeDisposable.add(
            hackerNewsRepositoryImpl.getArticle(id).observeOn(scheduler.ui())
                .subscribeOn(scheduler.io()).subscribe({
                    mView.hideLoading()
                    mView.getArticle(it)
                }, {
                    mView.hideLoading()
                    mView.showError(it.message)
                })
        )
    }

    override fun getComment(id: Int) {
        mView.showLoading()
        compositeDisposable.add(
            hackerNewsRepositoryImpl.getComment(id).observeOn(scheduler.ui())
                .subscribeOn(scheduler.io()).subscribe({
                    mView.hideLoading()
                    mView.getCommentsList(it)
                }, {
                    mView.hideLoading()
                    mView.showError(it.message)
                })
        )
    }

    override fun onDestroyPresenter() {
        compositeDisposable.dispose()
    }

    override fun insertFavStory(id: Int) {
        localRepositoryImpl.insertData(id)
    }

    override fun checkIsFavStory(id: Int) {
        mView.setFavoriteState(localRepositoryImpl.checkIsFavorite(id))
    }

    override fun deleteFavStory(id: Int) {
        localRepositoryImpl.deleteFavorite(id)
    }
}