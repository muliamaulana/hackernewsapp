package com.muliamaulana.hackernews.repository

import com.muliamaulana.hackernews.apiservices.response.ArticleResponse
import com.muliamaulana.hackernews.apiservices.response.CommentResponse
import io.reactivex.Flowable

interface HackerNewsRepository {
    fun getTopStories(): Flowable<List<Int>>
    fun getArticle(id: Int): Flowable<ArticleResponse>
    fun getComment(id: Int): Flowable<CommentResponse>
}