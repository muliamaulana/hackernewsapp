package com.muliamaulana.hackernews.repository

import com.muliamaulana.hackernews.database.FavoriteStory

interface LocalRepository {

    fun getFavoritesFromDB(): List<FavoriteStory>

    fun insertData(id: Int)

    fun checkIsFavorite(id: Int): List<FavoriteStory>

    fun deleteFavorite(id: Int)


}