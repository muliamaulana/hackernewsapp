package com.muliamaulana.hackernews.repository

import com.muliamaulana.hackernews.apiservices.HackerNewsServices
import com.muliamaulana.hackernews.apiservices.response.ArticleResponse
import com.muliamaulana.hackernews.apiservices.response.CommentResponse
import io.reactivex.Flowable

class HackerNewsRepositoryImpl(private val hackerNewsServices: HackerNewsServices) : HackerNewsRepository {

    override fun getTopStories(): Flowable<List<Int>> = hackerNewsServices.getTopStories()
    override fun getArticle(id: Int): Flowable<ArticleResponse> = hackerNewsServices.getArticle(id)
    override fun getComment(id: Int): Flowable<CommentResponse> = hackerNewsServices.getComment(id)


}