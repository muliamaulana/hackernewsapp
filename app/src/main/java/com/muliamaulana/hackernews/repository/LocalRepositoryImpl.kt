package com.muliamaulana.hackernews.repository

import android.content.Context
import com.muliamaulana.hackernews.database.FavoriteStory
import com.muliamaulana.hackernews.database.database
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select


class LocalRepositoryImpl(private val context: Context) : LocalRepository {

    override fun getFavoritesFromDB(): List<FavoriteStory> {
        lateinit var favoriteStoryList: List<FavoriteStory>
        context.database.use {
            val result = select(FavoriteStory.TABLE_FAVORITE_STORY)
            val favorit = result.parseList(classParser<FavoriteStory>())
            favoriteStoryList = favorit
        }
        return favoriteStoryList
    }

    override fun insertData(id: Int) {
        context.database.use {
            insert(FavoriteStory.TABLE_FAVORITE_STORY, FavoriteStory.STORY_ID to id)
        }
    }

    override fun checkIsFavorite(id: Int): List<FavoriteStory> {
        return context.database.use {
            val result = select(FavoriteStory.TABLE_FAVORITE_STORY)
                .whereArgs("(STORY_ID = {id})", "id" to id)
            val favorite = result.parseList(classParser<FavoriteStory>())
            favorite
        }
    }

    override fun deleteFavorite(id: Int) {
        context.database.use {
            delete(
                FavoriteStory.TABLE_FAVORITE_STORY, "(STORY_ID = {id})",
                "id" to id
            )
        }
    }
}