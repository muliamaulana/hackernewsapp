package com.muliamaulana.hackernews.apiservices

import com.muliamaulana.hackernews.BuildConfig.API_VERSION
import com.muliamaulana.hackernews.BuildConfig.BASE_URL
import com.muliamaulana.hackernews.apiservices.response.ArticleResponse
import com.muliamaulana.hackernews.apiservices.response.CommentResponse
import io.reactivex.Flowable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface HackerNewsServices {

    @GET("topstories.json?print=pretty")
    fun getTopStories(): Flowable<List<Int>>

    @GET("item/{id}.json?print=pretty")
    fun getArticle(@Path("id") id: Int): Flowable<ArticleResponse>

    @GET("item/{id}.json?print=pretty")
    fun getComment(@Path("id") id: Int): Flowable<CommentResponse>

    companion object {
        fun getHackerNewsServices(): HackerNewsServices {
            val client = OkHttpClient.Builder().build()

            val retrofit = Retrofit.Builder().baseUrl(BASE_URL + API_VERSION)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(HackerNewsServices::class.java)
        }
    }
}