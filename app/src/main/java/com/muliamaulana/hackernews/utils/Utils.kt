package com.muliamaulana.hackernews.utils

import java.text.SimpleDateFormat
import java.util.*

object Utils {

    fun getDateTime(unixTime: Int): String {
        val date = Date(unixTime * 1000L)
        val sdf = SimpleDateFormat("dd MMM yyyy | HH:mm", Locale.getDefault())
        val formattedDate = sdf.format(date)
        return formattedDate
    }
}