package com.muliamaulana.hackernews.database

data class FavoriteStory(val id: Long?, val idStory: Int) {

    companion object {
        const val TABLE_FAVORITE_STORY: String = "TABLE_FAVORITE_STORY"
        const val ID: String = "ID_"
        const val STORY_ID: String = "STORY_ID"
    }
}
