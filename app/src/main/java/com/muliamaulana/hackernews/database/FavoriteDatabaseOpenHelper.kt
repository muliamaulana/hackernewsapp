package com.muliamaulana.hackernews.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class FavoriteDatabaseOpenHelper(context: Context) :
    ManagedSQLiteOpenHelper(context, "FavoriteStory.db", null, 1) {

    companion object {
        private var instance: FavoriteDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(context: Context): FavoriteDatabaseOpenHelper {
            if (instance == null) {
                instance = FavoriteDatabaseOpenHelper(context.applicationContext)
            }
            return instance as FavoriteDatabaseOpenHelper
        }
    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.createTable(
            FavoriteStory.TABLE_FAVORITE_STORY, true,
            FavoriteStory.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavoriteStory.STORY_ID to INTEGER + UNIQUE
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(FavoriteStory.TABLE_FAVORITE_STORY, true)
    }
}

//Access property for context
val Context.database: FavoriteDatabaseOpenHelper
    get() = FavoriteDatabaseOpenHelper.getInstance(applicationContext)