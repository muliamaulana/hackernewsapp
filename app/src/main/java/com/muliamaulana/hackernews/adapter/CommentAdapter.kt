package com.muliamaulana.hackernews.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muliamaulana.hackernews.R
import com.muliamaulana.hackernews.apiservices.response.CommentResponse
import com.muliamaulana.hackernews.utils.Utils
import kotlinx.android.synthetic.main.comment_item.view.*


class CommentAdapter(
    private val list: MutableList<CommentResponse>,
    private val context: Context
) : RecyclerView.Adapter<CommentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.comment_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.textview_comment_name.text = list[position].by
        if (list[position].time != null) {
            holder.itemView.textview_comment_time.text = Utils.getDateTime(list[position].time!!)
        }
        holder.itemView.textview_comment_text.text = list[position].text
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)


}