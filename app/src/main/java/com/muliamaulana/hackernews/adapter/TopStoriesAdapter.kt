package com.muliamaulana.hackernews.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muliamaulana.hackernews.R
import com.muliamaulana.hackernews.apiservices.response.ArticleResponse
import com.muliamaulana.hackernews.ui.detailstory.DetailStoryActivity
import kotlinx.android.synthetic.main.top_stories_item.view.*


class TopStoriesAdapter(
    private val list: MutableList<ArticleResponse>,
    private val context: Context
) : RecyclerView.Adapter<TopStoriesAdapter.ViewHolder>() {

    private val sharedPreferences = context.getSharedPreferences("last_viewed_story", Context.MODE_PRIVATE)
    private val editor = sharedPreferences.edit()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.top_stories_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.textview_title.text = list[position].title
        holder.itemView.textview_score.text = "Score " + list[position].score.toString()
        val countOfComment = list[position].kids?.size
        holder.itemView.textview_count_of_comment.text = "$countOfComment Comments"
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailStoryActivity::class.java)
            intent.putExtra("id", list[position].id)
            context.startActivity(intent)
            editor.putInt("last_id", list[position].id!!)
            editor.commit()
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)


}